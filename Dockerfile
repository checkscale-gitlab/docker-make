FROM docker:git
LABEL maintainer="Julien Lecomte <julien@lecomte.at>"

RUN apk add --update --no-cache --no-progress \
      bash \
      curl \
      gettext \
      jq \
      make \
    && rm -fr /var/lib/apk /var/cache/apk \
    ;


