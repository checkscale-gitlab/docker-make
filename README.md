# docker-make

A Docker image that contains just the basic tools to build other Dockers with Makefile or scripts. It's main usage is within a continuous integration script.

## Building

Building is as easy as just running `make`, with tag being the branch name:

~~~bash
make
~~~

## Downloading

`docker-make` can be downloaded from Docker Hub or GitLab:

~~~bash
docker pull julienlecomte/docker-make
# or
docker pull registry.gitlab.com/jlecomte/images/docker-make
~~~

## Contents

This docker inherits from [`docker:git`](https://hub.docker.com/_/docker), which inherits itself from `docker:stable` and adds:
  * `make`: to enable building _Dockerfiles_ via _Makefiles_
  * `gettext`: to have `envsubst` to alter _Dockerfiles_ on the fly (without using Docker _build-args_)
  * `git`: to be able to read current repository
  * `curl`: to be able to trigger webhooks
  * `jq`: to be able to parse a JSON response
  * `bash`: to have a shell other than `busybox`

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Locations

  * GitLab: [https://gitlab.com/jlecomte/images/docker-make](https://gitlab.com/jlecomte/images/docker-make)
  * Docker hub: [https://hub.docker.com/r/julienlecomte/docker-make](https://hub.docker.com/r/julienlecomte/docker-make)

